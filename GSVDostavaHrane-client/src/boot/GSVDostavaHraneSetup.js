// import Vue from 'vue'

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

const firebaseConfig = {
  apiKey: 'AIzaSyCOf_MuMujEiOQIEwx4fI1viWosDqekllg',
  authDomain: 'gsv-dostava-hrane.firebaseapp.com',
  databaseURL: 'https://gsv-dostava-hrane.firebaseio.com',
  projectId: 'gsv-dostava-hrane',
  storageBucket: 'gsv-dostava-hrane.appspot.com',
  messagingSenderId: '518804273389',
  appId: '1:518804273389:web:378a7ea19de8cbd91834e4',
  measurementId: 'G-0VXXP8CKLB'
}

firebase.initializeApp(firebaseConfig)

export default ({ Vue }) => {
  Vue.prototype.$auth = firebase.auth()
  Vue.prototype.$db = firebase.firestore()
  Vue.prototype.$storage = firebase.storage()
}
