
const routes = [
  {
    path: '/',
    component: () => import('layouts/LoginPageLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Login/LoginIndex.vue') }
    ]
  },
  {
    path: '/Administration',
    component: () => import('layouts/GSVDostavaHraneLayout.vue'),
    meta: { auth: true },
    children: [
      { path: '/Restorani', meta: { auth: true }, component: () => import('pages/GSVDostavaHrane/RestoraniIndex.vue') },
      { path: '/Home', meta: { auth: true }, component: () => import('pages/GSVDostavaHrane/Home.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
