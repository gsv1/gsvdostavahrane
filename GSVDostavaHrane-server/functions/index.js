const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var admin = require("firebase-admin");

var serviceAccount = require("./gsv-dostava-hrane-firebase-adminsdk-ipmon-ed9f077e98.json");

var models = require('./models.js')

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://gsv-dostava-hrane.firebaseio.com"
});

const db = admin.firestore();

app.get('/dostava', async (request, response) => {
    let id = (
        typeof request.query.id !== 'undefined'
            ? request.query.id
            : null
    )
    let subCollection = (
        typeof request.query.subCollection !== 'undefined'
            ? request.query.subCollection
            : null
    )
    let order = null
    let where = null
    if (id === null) {
        order = models.getOrder(request.query)
        where = models.getWhere(request.query)
    }
    models.get(db, 'restorani', id, subCollection, order, where)
        .then(res => {
            return response.send(res)
        }).catch((error) => {
            return response.send(error)
        })
})

app.post('/dostava', (request, response) => {
    if (Object.keys(request.body).length) {
        db.collection('restorani').doc().set(request.body)
        .then(function () {
            return response.send(
                "Dokument uspješno kreiran!"
            )
        })
        .catch(function (error) {
            return response.send(
                "Greška pri pisanju u dokument: " + error
            )
        })
    } else {
        return response.send(
            "Nema post podatka za novi dokument. " + 
            "Novi dokument nije kreiran!"
        )
    }
})

app.put('/dostava', (request, response) => {
    if (Object.keys(request.body).length) {
        if (typeof request.query.id !== 'undefined') {
            db.collection('restorani')
                .doc(request.query.id)
                .set(request.body)
                .then(function() {
                    return response.send(
                        "Dokument uspješno izmjenjen - " + 
                        "ažurirano!"
                    )
                })
                .catch(function (error) {
                    return response.send(
                        "Pogreška u pisanju dokumenta: " + error
                    )
                })
        } else {
            return response.send(
                "Parametar nije upisan." + 
                "Dokument nije ažuriran!"
            )
        }
    } else {
        return response.send(
            "Nema post podatka za novi dokument. " +
            "Dokument nije ažuriran!"
        )
    }
})

app.delete('/dostava', (request, response) => {
    if (typeof request.query.id !== 'undefined') {
        db.collection('restorani').doc(request.query.id).delete()
            .then(function () {
                return response.send(
                    "Dokument uspješno izbrisan!"
                )
            })
            .catch(function (error) {
                return response.send(
                    "Pogreška pri brisanju dokumenta: " + error
                )
            })
    } else {
        return response.send(
            "Parametar nije upisan. " + 
            "Dokument nije izbrisan!"
        )
    }
})

app.listen(3000, () => {
    console.log("Server running on port 3000");
});